# libraries ----
library(gdata)
library(vioplot)
library(ggplot2)
library(reshape2)
library(beeswarm)
library(multcomp)
library(RColorBrewer)
library(lme4)

# options ----
options(digits=3)

getwd()
# setwd("/Users/dirk/Dropbox/801 X Myc/Figure IgG R")


# functions 
AnscombeTransform <- function(exosome){
  exosome$pfuAnscombe <- 2 * sqrt(exosome$pfu + 3/8)
  return(exosome)
}
dataCleaning <- function(exosome){
  head(exosome)
  ncol(exosome)
  colnames(exosome)
  exosome <- exosome[,1:12]
  
  summary(exosome)
  colnames(exosome)
  
  # exosome$DATE <- as.factor(exosome$DATE)
  
  levels(exosome$Sample)
  exosome <- exosome[exosome$Sample != "", ]
  exosome <- na.omit(exosome) 
  exosome <- drop.levels(exosome)
  
  levels(exosome$Class)
  exosome$Class <- relevel(exosome$Class, ref = "negative")

  colnames(exosome)
  colnames(exosome)[6] <- "cpFAM"
  colnames(exosome)[8] <- "cpVIC"
  
  return(exosome)
}
exosomeTechnicalReplicates <- function(exosome){
  # step 1 mean of technical replicates
  exosomeAve <- exosome
  attach(exosomeAve)
    exosomeAve$pfuAnscombe <- ave(pfuAnscombe, DATE, CONDITION, MOI, DILUTION, 
                             FUN = median)
  detach(exosomeAve)
  summary(exosomeAve)
  
  # remove technical replicates on the same plate
  nrow(exosomeAve)
  exosomeAve <- unique(exosomeAve)
  nrow(exosomeAve)
  return(exosomeAve)
}

exosomeZeroNormalized <- function(exosome.tech){
  exosomeWide <- dcast(exosome.tech, ... ~ condition, fun.aggregate = mean)
  colnames(exosomeWide)
  exosomeWide[,"Dose1"] <- exosomeWide[,"Dose1"] - exosomeWide[,"Dose0"]
  exosomeWide[,"Dose2"] <- exosomeWide[,"Dose2"] - exosomeWide[,"Dose0"]
  exosomeWide[,"Dose3"] <- exosomeWide[,"Dose3"] - exosomeWide[,"Dose0"]
  exosomeWide[,"Dose0"] <- exosomeWide[,"Dose0"] - exosomeWide[,"Dose0"]

  exosomeWide <- melt(exosomeWide)
  summary(exosomeWide)
  
  # rename columns
  colnames(exosomeWide)
  colnames(exosomeWide)[8] <- "condition"
  colnames(exosomeWide)[9] <- "EdUcount"
  
  # remove more duplicated data
  exosomeWide <- exosomeWide[-2]
  nrow(exosomeWide)
  exosomeWide <- unique(exosomeWide)
  nrow(exosomeWide)
  return(exosomeWide)
}
addFloorZero <- function(exosomeAve){
    if (exosomeAve < 1)   return(1)
  return(exosomeAve)
}

figure1 <- function(rain){
  
  a <-ggplot(aes(x = Class, y = cp, color = Cell), 
             data = rain) 
  a <- a + geom_boxplot(aes(x = Class, y = cp), 
                        color = "gray", fill = "gray",
                        outlier.colour = "white")
  a <- a + geom_point (size = I(11), 
                       alpha = I(0.5),
                       position = position_jitter(w = 0.2, h = 0.0))

  a <- a + scale_colour_brewer(type = "qual", palette = "Set1")
  a <- a + theme_bw() 
  a <- a + xlab ("\n Cell") 
  a <- a + ylab ("copies per diploid genome\n") 
  a <- a + labs(title = "KSHV\n")
  
  a <- a + scale_y_log10(limits = c(0.1, 100), breaks=c(0.1,1,10,100))
  
  
  # a <- a + facet_wrap( ~ genotype, scales = "free_y")  
  
  a <- a + theme(legend.position = "right") 
  a <- a + theme(strip.background = element_rect(colour = "white", fill = "white"))   
  a <- a + theme(axis.text.x = element_text(size = 24, angle = 90, hjust = 1)) 
  a <- a + theme(axis.title.x = element_text(size = 24))
  a <- a + theme(axis.text.y = element_text(size = 24, angle = 0)) 
  a <- a + theme(axis.title.y = element_text(size = 24, angle = 90)) 
  a <- a + theme(plot.title = element_text(size = 24))  
  a <- a  + theme(strip.text.x = element_text(size = 24, vjust = 1))
  a <- a  + theme(strip.text.y = element_text(size = 24))
  a <- a + theme(panel.grid.minor = element_blank()) 
  # a <- a + theme(panel.grid.major = element_blank())
  return (a)
}


# DATA READ ----
# 
exosome <- read.delim("toR.txt")
summary(exosome)
head(exosome)


# SAFE COPY OF DATASET ----
rainall <- exosome
write.table(rainall,file = "CompleteDataSet.txt", sep = "\t")
exosome <- rainall


# Data cleaning ----
exosome <- dataCleaning(exosome)
summary(exosome)


exosome$cpFAM <- sapply(exosome$cpFAM, FUN = addFloorZero)
exosome$cpVIC <- sapply(exosome$cpVIC, FUN = addFloorZero)
exosome$cp <- exosome$cpFAM/exosome$cpVIC    
    # adjustment to copies per cell

levels(exosome$Cell)
levels(exosome$Cell) <- c("BC3","CRO-AP6.a","CRO-AP6.b")
# exosome <- exosome[exosome$Cell != "CRO-AP3",]   # as CRO-AP3 are the same as CRO-AP6
exosome <- drop.levels(exosome)
 
#
# Figure 1 ---
figure1(exosome)

png(file = "plot1.png", 
    units = "in", 
    width = 8, 
    height = 10, res = 600)
figure1(exosome)
dev.off()

# Multiple comparison ----
rain <- exosome
rain <- drop.levels(rain)
summary(rain)

rain$Class <- relevel(rain$Class, ref = "negative")


# linear model
result <- lm(cp ~ Class + Cell, data = rain)
summary(result)
anova(result)
pf(q=47.01, df1=5, df2=12, lower.tail=FALSE)


# anova
result <- aov(cp ~ Class + Cell, 
              data = rain)
summary(result)
TukeyHSD(result)$Class

result.pair <- glht(result, linfct = mcp(Class="Tukey"))
summary(result.pair)

result.pair <- glht(result, linfct = mcp(Class="Dunnett"))
summary(result.pair)
confint(result.pair, 
        level = 0.95)



